// #################################
// # Author: Arisa Kubota
// # Email: arisa.kubota at cern.ch
// # Project: Yarr
// # Description: Get plot data from directory
// ################################

#include "PlotStyle.h"
#include "PlotTool.h"
#include "Histo1D.h"
#include "Histo2D.h"
#include "RootFile.h"
/////

void printHelp();

/// main function
int main(int argc, char *argv[]) {

    SetPlotStyle();

    /// Information
    std::cout << "\nThis command converts file data in the input directory into ROOT file and pdf file.\n" << std::endl;

    /// variables
    std::string i_dir    = "";
    std::string o_dir    = "";
    std::string ext      = "png";
    std::string par_file = "";
    bool doPrint         = false;

    // get argument
    int c;
    while ((c = getopt(argc, argv, "HhPi:o:e:p:")) != -1) {
        switch (c) {
            case 'H':
                printHelp();
                return 0;
                break;
            case 'h':
                printHelp();
                return 0;
                break;
            case 'P':
                doPrint = true;
                break;
            case 'i':
                i_dir = std::string(optarg);
                break;
            case 'o':
                o_dir = std::string(optarg);
                break;
            case 'e':
                ext = std::string(optarg);
                break;
            case 'p':
                par_file = std::string(optarg);
                break;
            case '?':
                if(optopt=='o'||optopt=='e'||optopt=='p'){
                    std::cerr <<
                        "\033[33m-> Option " <<
                        (char)optopt <<
                        " requires a parameter! (Proceeding with default)\033[m" <<
                    std::endl;
                }else if(optopt=='i'){
                    std::cerr <<
                        "\033[31m-> Option " <<
                        (char)optopt <<
                        " requires a parameter! Aborting...\033[m" <<
                    std::endl;
                    return -1;
                } else {
                    std::cerr << "\033[33m-> Unknown parameter: " << (char)optopt << "\033[m" << std::endl;
                }
                break;
            default:
                std::cerr << "\033[31m-> Error while parsing command line parameters!\033[m" << std::endl;
                return -1;
        }
    }

    ///////////////////
    /// Input directory
    if (i_dir=="") {
        std::cerr << "\033[31mError: No result directory given!\033[m" << std::endl;
        std::cerr << "\033[31m       Please specify result directory path under -i option.\033[m" << std::endl;
        return -1;
    }

    /// Remove trailing slash
    if (i_dir[i_dir.size()-1]=='/') i_dir = i_dir.substr(0, i_dir.size()-1);

    /// Open result directroy
    DIR *dp { nullptr };
    dp = opendir(i_dir.c_str());
    if ( !dp ) {
        std::cerr << "\033[31mError: Not found directory: " << i_dir << "\033[m" << std::endl;
        return -1;
    }

    ////////////////////
    /// Output directory
    if (o_dir=="") {
        std::cout << "\033[33mWarning: No output directory given. Proceeding with default: " << i_dir << "\033[m" << std::endl;
        o_dir = i_dir;
    }

    /// Remove trailing slash
    if (o_dir[o_dir.size()-1]=='/') o_dir = o_dir.substr(0, o_dir.size()-1);

    /// Specify output ROOT file
    std::string o_file = o_dir + "/rootfile.root";

    /// Make output directory if not exist
    std::string cmd = "mkdir -p " + o_dir;
    dp = opendir(o_dir.c_str());
    if (system(cmd.c_str())!=0) {
        std::cerr << "\033[31mError: Problem in creating " << o_dir << "\033[m" << std::endl;
        return -1;
    } else if (dp) {
        std::cout << "\033[33mWarning: Already exist directory: " << o_dir << "\033[m" << std::endl;
    }

#ifdef DEBUG
    ////////////////
    /// Confirmation
    std::cout << std::endl;
    std::cout << "*********************************" << std::endl;
    std::cout << "Input directory: "  << i_dir << std::endl;
    std::cout << "Output directory: " << o_dir << std::endl;
    std::cout << "Output Root file: " << o_file << std::endl;
    if (doPrint) std::cout << "Extension: " << ext << std::endl;
    if (par_file!="") std::cout << "Parameter file: " << par_file << std::endl;
    std::cout << "*********************************" << std::endl;
    std::cout << std::endl;
#endif

    //////////////////
    /// Make ROOT file
    std::unique_ptr<RootFile> f ( nullptr );

    std::cout << "Info: Generating ROOT file: " << o_file << "..." << std::endl;

    f.reset( new RootFile() );
    f->recreate(o_file);

    /// write scanLog
    if (f->writeScanLog(i_dir)!=0) return -1;

    /// write scanCfg
    if (f->writeScanCfg(i_dir)!=0) return -1;

    /// write chip data (config and result)
    if (f->writeChipData(i_dir)!=0) return -1;

    /////////////////////
    /// Analyze ROOT file
    std::string i_file = o_file;

    f.reset( new RootFile() );

    /// load input ROOT file/recreate output ROOT file
    if (f->load(i_file)!=0) return -1;

    /// print setting
    if (doPrint) {
        std::cout << "Info: Generating plots in directory: " << o_dir << "..." << std::endl;
        f->setPrint(ext);
    }
    f->recreate(o_file, o_dir);

    /// load scanLog from input file and write it into output file
    if (f->loadScanLog()!=0) return -1;

    /// load scanCfg from input file and write it into output file
    if (f->loadScanCfg()!=0) return -1;

    /// load chip data from input file and write it into output file
    if (f->loadChipData()!=0) return -1;

    std::cout << "Info: Finish.\n" << std::endl;

    return 0;
}

void printHelp() {
    std::cout << "Help:" << std::endl;
    std::cout << " -h/-H     : Shows this." << std::endl;
    std::cout << " -P        : Set the print mode True." << std::endl;
    std::cout << " -i <dir>  : Result directory." << std::endl;
    std::cout << " -o <dir>  : Output directory. (Default. path/to/input/dir)" << std::endl;
    std::cout << " -e <ext>  : Extension. (Default. png)" << std::endl;
    std::cout << " -p <json> : Parameter config file." << std::endl;
}
