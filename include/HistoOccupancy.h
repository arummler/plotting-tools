#ifndef __HISTOOCCUPANCY_H
#define __HISTOOCCUPANCY_H

#include "Histo1D.h"

class HistoOccupancy : public Histo1D
{
public:
    HistoOccupancy();
    ~HistoOccupancy();
private:
    void setParametersX(TH2* /*i_h*/);
    void drawHisto();
    void fillHisto(json& /*i_j*/);
    void fillHisto(TH2*  /*i_h*/);

    int whichBin(double /*value*/,
                 double /*occnum*/);
};
#endif //__HISTOOCCUPANCY_H
