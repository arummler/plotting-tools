#ifndef __ROOTFILE_H
#define __ROOTFILE_H

#include <memory>

#include <TFile.h>
#include <TDirectory.h>
#include <TObject.h>
#include <TH1.h>
#include <json.hpp>

using json=nlohmann::basic_json<std::map, std::vector, std::string, bool, std::int32_t, std::uint32_t, float>;

class RootFile
{
public:
    /// Constructor
    RootFile();

    /// Deconstructor
    ~RootFile();

    /***
    Recreate ROOT file.
    * i_path: path to ROOT file to recreate
    * i_dir: path to directory to output pdf file
    ***/
    void recreate(std::string /*i_path*/);
    void recreate(std::string /*i_path*/,
                  std::string /*i_dir*/);
    /***
    Load ROOT file.
    * i_path: path to ROOT file to load
    ***/
    int load(std::string /*i_path*/);

    /***
    Set parameters to output image file.
    * i_ext: extension
    ***/
    void setPrint(std::string /*i_ext*/);

    /***
    Write data into ROOT file from data file.
    * i_dir: path to directory where file is located
    * i_num: chip order
    ***/
    int writeScanLog(std::string /*i_dir*/);
    int writeScanCfg(std::string /*i_dir*/);
    int writeChipData(std::string /*i_dir*/);

    /***
    Load data from ROOT file and analyze/write data into ROOT file
    ***/
    int loadScanLog();
    int loadScanCfg();
    int loadChipData();

private:
    /***
    Write TH1/TH2 data into ROOT file from json data
    * i_name: chip name
    * i_data: json data
    * i_d: mother diretory
    * i_fe: AFE type (for RD53A)
    ***/
    void writePlotFromJson(std::string /*i_name*/,
                           json&       /*i_data*/,
                           TDirectory* /*i_d*/,
                           std::string i_fe="");
    /***
    Write TH1/TH2 data into ROOT file from TDirectory
    * i_name: chip name
    * i_d_in: input TDirectory
    * i_d_out: output TDirectory
    * i_fe: AFE type (for RD53A)
    ***/
    void writePlotFromDir(std::string /*i_name*/,
                          TDirectory* /*i_d_in*/,
                          TDirectory* /*i_d_out*/,
                          std::string i_fe="");
    /***
    Write TH1/TH2 data into ROOT file from TObject
    * i_name: chip name
    * i_o: Object (TH1/TH2)
    * i_d: output TDirectory
    * i_type: data type
    * i_fe: AFE type (for RD53A)
    ***/
    void writePlotFromObj(std::string /*i_name*/,
                          TObject*    /*i_o*/,
                          TDirectory* /*i_d*/,
                          std::string /*i_type*/,
                          std::string i_fe="");

    /***
    Make TDirectory in ROOT file
    * i_mother: mother directory
    * i_name: directory name
    * i_title: type of data placed in directory
    ***/
    TDirectory* mkdir(TDirectory* /*i_mother*/,
                      std::string /*i_name*/,
                      std::string i_title = "");

    TFile* m_file;
    TFile* m_infile;
    bool m_output;
    bool m_input;

    std::string m_testtype;
    std::string m_chiptype;
    json m_chips;

    bool m_print;
    std::string m_ext;
    std::string m_dir;
};

#endif //__ROOTFILE_H
